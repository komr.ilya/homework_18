#include <iostream>

class Stack
{
public:
    Stack() : size(10), lastIndex(-1)
    {
        arr = new int[size];
    }
    ~Stack()
    {
        delete[] arr;
    }

    void Pop()
    {
        if (lastIndex > -1)
        {
            std::cout << "First item: " << arr[lastIndex] << std::endl;
            lastIndex--;
        }
        else
        {
            std::cout << "Warning: Array is empty!" << std::endl;
        }
    }

    void Push(int p)
    {
        if (lastIndex < size)
        {
            lastIndex++;
            arr[lastIndex] = p;
        }
        else
        {
            std::cout << "Warning: Array is full! The item '" << p << "' is not added." << std::endl;
        }
        
    }

    void ShowAll()
    {
        if (lastIndex > -1)
        {
            std::cout << "Show all array:" << std::endl;
            for (int i = 0; i <= lastIndex; i++)
            {
                std::cout << "item [" << i << "]: " << arr[i] << std::endl;
            }
        }
        else
        {
            std::cout << "Warning: Array is empty!" << std::endl;
        }
        
        
    }
private:
    int lastIndex;
    int size;
    int* arr;
};




int main()
{
    Stack s;
    s.Pop();
    s.Push(10);
    s.ShowAll();
    s.Pop();
    s.Push(2);
    s.Push(19);
    s.Push(11);
    s.Push(32);
    s.Pop();
    s.Push(7);
    s.Push(22);
    s.Push(8);
    s.Pop();
    s.ShowAll();
    s.Push(3);
    s.Push(10);
    s.Push(15);
    s.Push(23);
    s.Push(90);
    s.Push(25);
    s.Push(55);
    s.Push(66);
    s.ShowAll();
}
